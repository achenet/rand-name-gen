package main

import (
	"fmt"
	"log"
	"math/rand"
	"os"
	"strings"
	"unicode/utf8"
)

// TODO min and max length as flags

// TODO specify input

func main() {
	// select random number for length
	n := rand.Intn(7) + 5
	buf, err := os.ReadFile("input.txt")
	if err != nil {
		log.Fatalf("error reading input file: %s", err.Error())
	}
	input := strings.ReplaceAll(string(buf), " ", "")
	input = strings.ReplaceAll(input, ",", "")
	input = strings.ReplaceAll(input, "'", "")
	input = strings.ReplaceAll(input, "\"", "")
	// TODO use regex
	ft := GetFreqTable(input)
	tm := GetTransitionMatrix(input)
	out := make([]rune, n)
	for i := range out {
		if i < 1 {
			out[i] = ft.GetLetter()
			continue
		}
		out[i] = tm.GetNextLetter(out[i-1])
	}
	fmt.Printf("random name: %s\n", string(out))
}

type FreqTable map[rune]float32

func GetFreqTable(text string) FreqTable {
	out := FreqTable{}
	for _, char := range text {
		out[char] += 1.0
	}
	numRunes := utf8.RuneCountInString(text)
	for char, val := range out {
		out[char] = val / float32(numRunes)
	}
	return out
}

// GetLetter returns a letter from the frequency table
// with the probability of each letter being selected
// being equal to the frequency of that letter
func (f FreqTable) GetLetter() rune {
	x := rand.Float32()
	return f.getLetter(x)
}

// getLetter is the deterministic function
// that returns a given letter given a number
// it is only called by GetLetter, so we can be
// sure that x is between zero and one
// it is split into a seperate function
// for easier testing
func (f FreqTable) getLetter(x float32) rune {
	var sum float32
	for letter, proba := range f {
		sum += proba
		if sum > x {
			return letter
		}
	}
	// this should never happen
	return 0
}

type TransitionMatrix map[rune]FreqTable

// GetTransitionMatrix returns a transition matrix
// with the transition probabilities found in the
// text given as argument
func GetTransitionMatrix(text string) TransitionMatrix {
	out := TransitionMatrix{}
	if len(text) < 2 {
		return out
	}
	runes := toRuneSlice(text)
	for _, char := range runes {
		// skip entries that exist already
		if _, ok := out[char]; ok {
			continue
		}
		out[char] = transitionsFor(char, runes)
	}
	return out
}

func transitionsFor(char rune, text []rune) FreqTable {
	out := FreqTable{}
	for i, r := range text {
		if r != char || i >= len(text)-1 {
			continue
		}
		out[text[i+1]] += 1.0
	}
	for k, v := range out {
		out[k] = v / float32(len(out))
	}
	return out
}

func (t TransitionMatrix) GetNextLetter(current rune) rune {
	return t[current].GetLetter()
}

func toRuneSlice(text string) []rune {
	out := make([]rune, utf8.RuneCountInString(text))
	// use a seperate counter to avoid indexing errors
	i := 0
	for _, r := range text {
		out[i] = r
		i++
	}
	return out
}
