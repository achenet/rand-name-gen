package main

import "testing"

func TestGetFreqTable(t *testing.T) {
	for input, expect := range map[string]FreqTable{
		"a":  {'a': 1.0},
		"aa": {'a': 1.0},
		"ab": {'a': 0.5, 'b': 0.5},
		"ba": {'a': 0.5, 'b': 0.5},
	} {
		got := GetFreqTable(input)
		if !areEqual[rune](got, expect) {
			t.Errorf(
				"input: %s, got: %v, expect: %v",
				input, got, expect,
			)
		}
	}
}

func Test_getLetter(t *testing.T) {
	for _, tc := range []struct {
		t FreqTable
		m map[float32]rune
	}{
		{
			t: FreqTable{
				'a': 1.0,
			},
			m: map[float32]rune{
				0.5: 'a',
				0.7: 'a',
				0.1: 'a',
			},
		}, {
			t: FreqTable{
				'a': 0.9,
				'b': 0.1,
			},
			m: map[float32]rune{
				0.5:  'a',
				0.7:  'a',
				0.1:  'a',
				0.99: 'b',
			},
		},
	} {
		for input, expect := range tc.m {
			got := tc.t.getLetter(input)
			if got != expect {
				t.Errorf(
					"input: %v, got: %v, expect: %v",
					input, got, expect,
				)
			}
		}
	}
}

func TestGetTransitionMatrix(t *testing.T) {
	for input, expect := range map[string]TransitionMatrix{} {
		got := GetTransitionMatrix(input)
		if !areEqualTransitionMatrix(got, expect) {
			t.Errorf(
				"input: %s\ngot: %v\nexpect: %v",
				input, got, expect,
			)
		}
	}
}

func Test_transitionsFor(t *testing.T) {
	for _, tc := range []struct {
		char   rune
		text   []rune
		expect FreqTable
	}{
		{
			char: 'a',
			text: []rune{'a', 'b', 'a', 'c'},
			expect: FreqTable{
				'b': 0.5,
				'c': 0.5,
			},
		},
	} {
		got := transitionsFor(tc.char, tc.text)
		if !areEqual[rune](got, tc.expect) {
			t.Errorf(
				"char: %v\ntext: %v\ngot: %v\nexpect: %v",
				tc.char, tc.text, got, tc.expect,
			)
		}
	}
}

func areEqual[T comparable](a, b map[T]float32) bool {
	for k, v := range a {
		if b[k] != v {
			return false
		}
	}
	for k, v := range b {
		if a[k] != v {
			return false
		}
	}
	return true
}

func areEqualTransitionMatrix(got, expect TransitionMatrix) bool {
	// TODO
	return false
}
