# Random Name Generator

Generates random names

### Usage
Create an `input.txt` in the root of the directory.
This input will be used to generate the frequency tables and transition matrix for the random name generator.
In other words, the randomly generated name will be similar to input text.

To run on a computer with Go installed: `go run .`.
Program output will be written to stdout.
